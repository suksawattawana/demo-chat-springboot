package com.livemassage.configs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LiveMassageApplication {

	public static void main(String[] args) {
		SpringApplication.run(LiveMassageApplication.class, args);
	}

}
