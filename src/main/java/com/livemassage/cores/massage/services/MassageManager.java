package com.livemassage.cores.massage.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Component;

import com.livemassage.cores.massage.model.ChatMessage;

@Component
public class MassageManager {

	@Autowired
	private MassageService service;

	public ChatMessage register(ChatMessage chatMessage, SimpMessageHeaderAccessor headerAccessor) throws Exception {
		ChatMessage result = null;

		try {
			if (chatMessage != null) {
				result = service.register(chatMessage, headerAccessor);
			}
		} catch (Exception e) {
			throw e;
		}

		return result;
	}

	public ChatMessage sendMessage(ChatMessage chatMessage) {
		ChatMessage result = null;
		try {
			if (chatMessage != null) {
				result = service.sendMessage(chatMessage);
			}
		} catch (Exception e) {
			throw e;
		}
		return result;
	}
}
