package com.livemassage.cores.massage.services;

import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Service;

import com.livemassage.cores.massage.model.ChatMessage;

@Service
public class MassageService {

	public ChatMessage register(ChatMessage chatMessage, SimpMessageHeaderAccessor headerAccessor) throws Exception {
		ChatMessage result = null;
		try {
			result = chatMessage;
			headerAccessor.getSessionAttributes().put("username", result.getSender());
		} catch (Exception e) {
			throw e;
		}
		return result;
	}
	
	public ChatMessage sendMessage(ChatMessage chatMessage) {
		ChatMessage result = null;
		try {
			result = chatMessage;
		} catch (Exception e) {
			throw e;
		}
		return result;
	}
}
