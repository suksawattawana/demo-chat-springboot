package com.livemassage.enums;

public enum MessageType {
	CHAT, LEAVE, JOIN
}
